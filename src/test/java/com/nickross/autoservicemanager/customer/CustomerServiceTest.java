package com.nickross.autoservicemanager.customer;

import com.nickross.autoservicemanager.customer.model.Customer;
import com.nickross.autoservicemanager.customer.model.CustomerDAO;
import com.nickross.autoservicemanager.customer.model.CustomerDTO;
import com.nickross.autoservicemanager.exceptions.ValidationException;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.verify;
import static org.mockito.MockitoAnnotations.initMocks;

/**
 * @author Nick Ross on 2018-08-21.
 */
public class CustomerServiceTest {
    @Mock
    private CustomerDAO customerDAO;

    @InjectMocks
    private CustomerService customerService;

    @Before
    public void setup() {
        initMocks(this);
    }

    @Test(expected = ValidationException.class)
    public void addCustomer_malformedCustomerDTO_throwsException() throws Exception {
        CustomerDTO customerDTO = new CustomerDTO();
        customerService.addCustomer(customerDTO);
    }

    @Test
    public void addCustomer_wellformedCustomerDTO_callsCreate() throws Exception {
        CustomerDTO customerDTO = new CustomerDTO();
        customerDTO.setFirstName("Michele");
        customerDTO.setLastName("Obama");

        Customer returnedCustomer = new Customer();
        returnedCustomer.setFirstName("Michele");
        returnedCustomer.setLastName("Obama");
        doReturn(returnedCustomer).when(customerDAO).create(any(Customer.class));
        customerService.addCustomer(customerDTO);

        verify(customerDAO).create(any(Customer.class));
    }
}
