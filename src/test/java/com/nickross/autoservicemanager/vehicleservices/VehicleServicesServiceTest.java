package com.nickross.autoservicemanager.vehicleservices;

import com.nickross.autoservicemanager.exceptions.ValidationException;
import com.nickross.autoservicemanager.services.ServicesService;
import com.nickross.autoservicemanager.services.model.Service;
import com.nickross.autoservicemanager.vehicle.VehicleService;
import com.nickross.autoservicemanager.vehicle.electric.model.ElectricVehicleDTO;
import com.nickross.autoservicemanager.vehicle.model.BaseVehicleDTO;
import com.nickross.autoservicemanager.vehicle.model.Vehicle;
import com.nickross.autoservicemanager.vehicleservices.model.VehicleServices;
import com.nickross.autoservicemanager.vehicleservices.model.VehicleServicesDAO;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.util.ArrayList;
import java.util.List;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.verify;

/**
 * @author Nick Ross on 2018-08-21.
 */
public class VehicleServicesServiceTest {

    @Mock
    private VehicleService vehicleServiceMock;
    @Mock
    private ServicesService servicesService;
    @Mock
    private VehicleServicesDAO vehicleServicesDAO;

    @InjectMocks
    private VehicleServicesService vehicleServicesService;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
    }

    @Test(expected = ValidationException.class)
    public void addServicesForVehicle_vehicleDoesntExist_throwsValidationException() throws Exception {
        List<String> errors = new ArrayList<>();
        errors.add("Missing vehicle");
        doThrow(new ValidationException(errors)).when(vehicleServiceMock).validateVehicle(anyLong());

        vehicleServicesService.addServicesForVehicle(1L, new ArrayList<>());
    }

    @Test(expected = ValidationException.class)
    public void addServicesForVehicle_typedVehicleDoesntExist_throwsValidationException() throws Exception {
        List<String> errors = new ArrayList<>();
        errors.add("Vehicle not found");
        Vehicle returnedVehicle = new Vehicle();
        doReturn(returnedVehicle).when(vehicleServiceMock).validateVehicle(anyLong());
        doThrow(new ValidationException(errors)).when(vehicleServiceMock).getTypedVehicleByVehicle(eq(returnedVehicle));

        vehicleServicesService.addServicesForVehicle(1L, new ArrayList<>());
    }

    @Test(expected = ValidationException.class)
    public void addServicesForVehicle_invalidService_throwsValidationException() throws Exception {
        List<String> errors = new ArrayList<>();
        errors.add("Invalid service");
        Vehicle returnedVehicle = new Vehicle();
        BaseVehicleDTO returnedBaseVehiclDTO = new ElectricVehicleDTO();
        doReturn(returnedVehicle).when(vehicleServiceMock).validateVehicle(anyLong());
        doReturn(returnedBaseVehiclDTO).when(vehicleServiceMock).getTypedVehicleByVehicle(eq(returnedVehicle));
        doThrow(new ValidationException(errors)).when(servicesService).validateService(anyLong());

        List<Long> servicesToAdd = new ArrayList<>();
        servicesToAdd.add(1L);
        vehicleServicesService.addServicesForVehicle(1L, servicesToAdd);
    }

    @Test(expected = ValidationException.class)
    public void addServicesForVehicle_serviceNotApplicableToVehicleType_throwsValidationException() throws Exception {
        Vehicle returnedVehicle = new Vehicle();
        BaseVehicleDTO returnedBaseVehiclDTO = new ElectricVehicleDTO();
        Service returnedService = new Service();
        returnedService.setElectricSpecific(false);
        returnedService.setDieselSpecific(false);
        returnedService.setGasSpecific(true);

        doReturn(returnedVehicle).when(vehicleServiceMock).validateVehicle(anyLong());
        doReturn(returnedBaseVehiclDTO).when(vehicleServiceMock).getTypedVehicleByVehicle(eq(returnedVehicle));
        doReturn(returnedService).when(servicesService).validateService(anyLong());

        List<Long> servicesToAdd = new ArrayList<>();
        servicesToAdd.add(1L);
        vehicleServicesService.addServicesForVehicle(1L, servicesToAdd);
    }

    @Test
    public void addServicesForVehicle_applicableServiceVehicleExists_callsCreate() throws Exception {
        Vehicle returnedVehicle = new Vehicle();
        BaseVehicleDTO returnedBaseVehiclDTO = new ElectricVehicleDTO();
        Service returnedService = new Service();
        returnedService.setElectricSpecific(true);
        returnedService.setDieselSpecific(false);
        returnedService.setGasSpecific(true);

        doReturn(returnedVehicle).when(vehicleServiceMock).validateVehicle(anyLong());
        doReturn(returnedBaseVehiclDTO).when(vehicleServiceMock).getTypedVehicleByVehicle(eq(returnedVehicle));
        doReturn(returnedService).when(servicesService).validateService(anyLong());

        List<Long> servicesToAdd = new ArrayList<>();
        servicesToAdd.add(1L);
        vehicleServicesService.addServicesForVehicle(1L, servicesToAdd);

        verify(vehicleServicesDAO).create(any(VehicleServices.class));
    }
}
