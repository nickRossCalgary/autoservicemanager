package com.nickross.autoservicemanager.vehicle;

import com.nickross.autoservicemanager.exceptions.ValidationException;
import com.nickross.autoservicemanager.vehicle.diesel.model.DieselVehicleDTO;
import com.nickross.autoservicemanager.vehicle.electric.model.ElectricVehicleDTO;
import com.nickross.autoservicemanager.vehicle.gas.model.GasVehicleDTO;
import com.nickross.autoservicemanager.vehicle.model.BaseVehicleDTO;
import com.nickross.autoservicemanager.vehicle.model.RequestVehicleDTO;
import org.junit.Test;

import static junit.framework.TestCase.assertTrue;

/**
 * @author Nick Ross on 2018-08-21.
 */
public class VehicleRequestHelperTest {

    @Test(expected = ValidationException.class)
    public void parseRequestVehicleToTypedVehicle_missingType_throwsException() throws Exception{
        RequestVehicleDTO requestVehicleDTO = new RequestVehicleDTO();

        VehicleRequestHelper.parseRequestVehicleToTypedVehicle(requestVehicleDTO);
    }

    @Test(expected = ValidationException.class)
    public void parseRequestVehicleToTypedVehicle_wrongType_throwsException() throws Exception{
        RequestVehicleDTO requestVehicleDTO = new RequestVehicleDTO();
        requestVehicleDTO.setType("chocolateChipCookie");

        VehicleRequestHelper.parseRequestVehicleToTypedVehicle(requestVehicleDTO);
    }

    @Test
    public void parseRequestVehicleToTypedVehicle_electricVehicle_returnsElectricDTO() throws Exception{
        RequestVehicleDTO requestVehicleDTO = new RequestVehicleDTO();
        requestVehicleDTO.setType("electric");
        requestVehicleDTO.setBatteryCapacity(1200);

        BaseVehicleDTO result = VehicleRequestHelper.parseRequestVehicleToTypedVehicle(requestVehicleDTO);

        assertTrue(result instanceof ElectricVehicleDTO);
    }
    @Test
    public void parseRequestVehicleToTypedVehicle_gasVehicle_returnsgasDTO() throws Exception{
        RequestVehicleDTO requestVehicleDTO = new RequestVehicleDTO();
        requestVehicleDTO.setType("gas");
        requestVehicleDTO.setOilType("synthetic");

        BaseVehicleDTO result = VehicleRequestHelper.parseRequestVehicleToTypedVehicle(requestVehicleDTO);

        assertTrue(result instanceof GasVehicleDTO);
    }
    @Test
    public void parseRequestVehicleToTypedVehicle_electricVehicle_returnsdiselDTO() throws Exception{
        RequestVehicleDTO requestVehicleDTO = new RequestVehicleDTO();
        requestVehicleDTO.setType("diesel");
        requestVehicleDTO.setDieselFilter("basic");

        BaseVehicleDTO result = VehicleRequestHelper.parseRequestVehicleToTypedVehicle(requestVehicleDTO);

        assertTrue(result instanceof DieselVehicleDTO);
    }
}
