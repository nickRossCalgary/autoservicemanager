package com.nickross.autoservicemanager.vehicle;

import com.nickross.autoservicemanager.customer.CustomerService;
import com.nickross.autoservicemanager.exceptions.ValidationException;
import com.nickross.autoservicemanager.vehicle.diesel.DieselVehicleService;
import com.nickross.autoservicemanager.vehicle.diesel.model.DieselVehicleDTO;
import com.nickross.autoservicemanager.vehicle.electric.ElectricVehicleService;
import com.nickross.autoservicemanager.vehicle.electric.model.ElectricVehicleDTO;
import com.nickross.autoservicemanager.vehicle.gas.GasVehicleService;
import com.nickross.autoservicemanager.vehicle.gas.model.GasVehicleDTO;
import com.nickross.autoservicemanager.vehicle.model.BaseVehicleDTO;
import com.nickross.autoservicemanager.vehicle.model.VehicleDAO;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;

import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.verify;
import static org.mockito.MockitoAnnotations.initMocks;

/**
 * @author Nick Ross on 2018-08-21.
 */
public class VehicleServiceTest {
    @Mock
    private ElectricVehicleService electricVehicleService;
    @Mock
    private DieselVehicleService dieselVehicleService;
    @Mock
    private GasVehicleService gasVehicleService;
    @Mock
    private CustomerService customerService;
    @Mock
    private VehicleDAO vehicleDAO;

    @InjectMocks
    private VehicleService vehicleService;

    @Before
    public void setup() {
        initMocks(this);
    }

    @Test(expected = ValidationException.class)
    public void addVehicle_unsupportedVehicleType_throwsValidationException() throws Exception {
        FlyingVehicleDTO unsupportedVehicleTypeDTO = new FlyingVehicleDTO();

        vehicleService.addVehicle(1L, unsupportedVehicleTypeDTO);
    }

    @Test
    public void addVehicle_supportedVehicle_callsAppropriateService() throws Exception {
        ElectricVehicleDTO electricVehicleDTO = new ElectricVehicleDTO();
        vehicleService.addVehicle(1L, electricVehicleDTO);
        verify(electricVehicleService).addElectricVehicle(eq(1L), eq(electricVehicleDTO));

        GasVehicleDTO gasVehicleDTO = new GasVehicleDTO();
        vehicleService.addVehicle(1L, gasVehicleDTO);
        verify(gasVehicleService).addGasVehicle(eq(1L), eq(gasVehicleDTO));

        DieselVehicleDTO dieselVehicleDTO = new DieselVehicleDTO();
        vehicleService.addVehicle(1L, dieselVehicleDTO);
        verify(dieselVehicleService).addDieselVehicle(eq(1L), eq(dieselVehicleDTO));
    }

    private class FlyingVehicleDTO extends BaseVehicleDTO {

    }
}
