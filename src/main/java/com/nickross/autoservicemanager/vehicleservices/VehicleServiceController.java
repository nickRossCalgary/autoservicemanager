package com.nickross.autoservicemanager.vehicleservices;

import com.nickross.autoservicemanager.data.UrlResourcePatterns;
import com.nickross.autoservicemanager.exceptions.ValidationException;
import com.nickross.autoservicemanager.vehicleservices.model.VehicleServicesDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * @author Nick Ross on 2018-08-20.
 */
@CrossOrigin(origins = {"http://localhost:3000"})
@RestController
@EnableAutoConfiguration
@RequestMapping(value = UrlResourcePatterns.VEHICLES + "/{id}/" + UrlResourcePatterns.SERVICES)
public class VehicleServiceController {

    @Autowired
    private VehicleServicesService vehicleServicesService;

    @GetMapping("")
    public ResponseEntity<Object> getServicesForVehicle(@PathVariable("id") long id){
        try{
            VehicleServicesDTO result = vehicleServicesService.getServicesForVehicle(id);
            return ResponseEntity.ok(result);
        }catch(ValidationException e){
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body(e.getErrors());
        }catch(Exception e){
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
        }
    }

    @PostMapping("")
    public ResponseEntity<Object> createServicesForVehicle(@PathVariable("id") long id,
                                                           @RequestBody List<Long> serviceIds){
        try{
            vehicleServicesService.addServicesForVehicle(id, serviceIds);
            return ResponseEntity.status(HttpStatus.CREATED).build();
        }catch(ValidationException e){
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(e.getErrors());
        }
        catch(Exception e){
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
        }
    }

    @DeleteMapping("/{serviceId}")
    public ResponseEntity<Object> removeServicesForVehicle(@PathVariable("id") long id,
                                                           @PathVariable("serviceId") Long serviceId){

        try{
            vehicleServicesService.removeServicesForVehicle(id, serviceId);
            return ResponseEntity.status(HttpStatus.CREATED).build();
        }catch(ValidationException e){
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(e.getErrors());
        }
        catch(Exception e){
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
        }
    }


}
