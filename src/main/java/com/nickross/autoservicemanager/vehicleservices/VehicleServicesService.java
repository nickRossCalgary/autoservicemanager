package com.nickross.autoservicemanager.vehicleservices;

import com.nickross.autoservicemanager.exceptions.ValidationException;
import com.nickross.autoservicemanager.services.ServicesService;
import com.nickross.autoservicemanager.services.model.ServiceDTO;
import com.nickross.autoservicemanager.vehicle.VehicleService;
import com.nickross.autoservicemanager.vehicle.diesel.model.DieselVehicleDTO;
import com.nickross.autoservicemanager.vehicle.electric.model.ElectricVehicleDTO;
import com.nickross.autoservicemanager.vehicle.gas.model.GasVehicleDTO;
import com.nickross.autoservicemanager.vehicle.model.BaseVehicleDTO;
import com.nickross.autoservicemanager.vehicle.model.Vehicle;
import com.nickross.autoservicemanager.vehicleservices.model.VehicleServices;
import com.nickross.autoservicemanager.vehicleservices.model.VehicleServicesDAO;
import com.nickross.autoservicemanager.vehicleservices.model.VehicleServicesDTO;
import org.apache.tomcat.jni.Local;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.function.Predicate;

/**
 * @author Nick Ross on 2018-08-20.
 */
@Transactional
@Service
public class VehicleServicesService {

    @Autowired
    private VehicleServicesDAO vehicleServicesDAO;
    @Autowired
    private ServicesService servicesService;
    @Autowired
    private VehicleService vehicleService;

    public VehicleServicesDTO getServicesForVehicle(Long id) throws ValidationException {
        //validate vehicle
        Vehicle vehicle = vehicleService.validateVehicle(id);
        List<VehicleServices> vs = vehicleServicesDAO.getAllForVehicle(vehicle);
        VehicleServicesDTO result = new VehicleServicesDTO();
        result.setVehicleDTO(vehicleService.getTypedVehicleByVehicle(vehicle));
        vs.forEach(v -> result.getVehicleServices().add(serviceEntityToDTO(v.getService(), v)));
        return result;
    }

    ServiceDTO serviceEntityToDTO(com.nickross.autoservicemanager.services.model.Service s, VehicleServices
            vehicleServices) {
        ServiceDTO result = new ServiceDTO();
        result.setId(vehicleServices.getId());
        result.setTitle(s.getTitle());
        result.setElectricSpecific(s.getElectricSpecific());
        result.setDieselSpecific(s.getDieselSpecific());
        result.setGasSpecific(s.getGasSpecific());

        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MMM-dd");
        String formatDateTime = vehicleServices.getDate() != null ? vehicleServices.getDate().format(formatter) : "";
        result.setDate(formatDateTime);

        return result;
    }

    @Transactional
    public void addServicesForVehicle(long vehicleId, List<Long> serviceIds) throws ValidationException {
        VehicleServices vehicleServices = new VehicleServices();
        //validateVehicle
        Vehicle vehicle = vehicleService.validateVehicle(vehicleId);
        vehicleServices.setVehicle(vehicle);
        BaseVehicleDTO v = vehicleService.getTypedVehicleByVehicle(vehicle);

        //validateServices
        for (Long serviceId : serviceIds) {
            com.nickross.autoservicemanager.services.model.Service s = servicesService.validateService(serviceId);
            if (!isServiceApplicable(v, s)) {
                List<String> errors = new ArrayList<>();
                errors.add("Incompatable service");
                throw new ValidationException(errors);
            }
            vehicleServices.setService(s);
            vehicleServices.setDate(LocalDateTime.now());
            vehicleServicesDAO.create(vehicleServices);
        }
    }

    private boolean isServiceApplicable(BaseVehicleDTO vehicleDTO,
                                        com.nickross.autoservicemanager.services.model.Service s) {
        if (!s.getDieselSpecific() && !s.getElectricSpecific() && !s.getGasSpecific()) {
            return true;
        }
        if (s.getDieselSpecific() && (vehicleDTO instanceof DieselVehicleDTO)) {
            return true;
        }
        if (s.getElectricSpecific() && (vehicleDTO instanceof ElectricVehicleDTO)) {
            return true;
        }
        if (s.getGasSpecific() && (vehicleDTO instanceof GasVehicleDTO)) {
            return true;
        }

        return false;
    }

    @Transactional
    public void removeServicesForVehicle(long vehicleId, Long serviceId) throws ValidationException {
        VehicleServices vehicleServices = new VehicleServices();
        //validateVehicle
        Vehicle vehicle = vehicleService.validateVehicle(vehicleId);
        vehicleServices.setVehicle(vehicle);

        Optional<VehicleServices> result = vehicleServicesDAO.getById(serviceId);
        result.ifPresent(vehicleServices1 -> vehicleServicesDAO.delete(vehicleServices1));
    }
}

