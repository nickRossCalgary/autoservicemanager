package com.nickross.autoservicemanager.vehicleservices.model;

import com.nickross.autoservicemanager.services.model.Service;
import com.nickross.autoservicemanager.vehicle.model.Vehicle;

import javax.persistence.*;
import java.sql.Date;
import java.time.LocalDateTime;

/**
 * @author Nick Ross on 2018-08-20.
 */
@Entity
@Table(name="vehicle_services")
@NamedQueries({
        @NamedQuery(name="VehicleServices.getByVehicle",
                query = "SELECT v FROM VehicleServices v WHERE v.vehicle = :vehicle"),
        @NamedQuery(name="VehicleServices.getById",
                query = "SELECT v FROM VehicleServices v WHERE v.id = :id")
})
public class VehicleServices {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @JoinColumn(name="vehicle_id")
    @ManyToOne
    private Vehicle vehicle;

    @JoinColumn(name="service_id")
    @ManyToOne
    private Service service;

    @Column(name="date")
    private LocalDateTime date;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Vehicle getVehicle() {
        return vehicle;
    }

    public void setVehicle(Vehicle vehicle) {
        this.vehicle = vehicle;
    }

    public Service getService() {
        return service;
    }

    public void setService(Service service) {
        this.service = service;
    }

    public LocalDateTime getDate() {
        return date;
    }

    public void setDate(LocalDateTime date) {
        this.date = date;
    }
}
