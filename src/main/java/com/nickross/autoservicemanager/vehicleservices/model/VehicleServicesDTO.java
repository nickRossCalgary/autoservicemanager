package com.nickross.autoservicemanager.vehicleservices.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.nickross.autoservicemanager.services.model.ServiceDTO;
import com.nickross.autoservicemanager.vehicle.model.BaseVehicleDTO;
import com.nickross.autoservicemanager.vehicle.model.Vehicle;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

/**
 * @author Nick Ross on 2018-08-20.
 */
public class VehicleServicesDTO {
    private Long id;
    @JsonProperty("vehicle")
    private BaseVehicleDTO vehicleDTO;
    @JsonProperty("services")
    private List<ServiceDTO> vehicleServices = new ArrayList<>();

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public BaseVehicleDTO getVehicleDTO() {
        return vehicleDTO;
    }

    public void setVehicleDTO(BaseVehicleDTO vehicleDTO) {
        this.vehicleDTO = vehicleDTO;
    }

    public List<ServiceDTO> getVehicleServices() {
        return vehicleServices;
    }

    public void setVehicleServices(List<ServiceDTO> vehicleServices) {
        this.vehicleServices = vehicleServices;
    }
}
