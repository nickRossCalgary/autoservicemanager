package com.nickross.autoservicemanager.vehicleservices.model;

import com.nickross.autoservicemanager.data.BaseDAO;
import com.nickross.autoservicemanager.vehicle.model.Vehicle;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

/**
 * @author Nick Ross on 2018-08-20.
 */
@Repository
public class VehicleServicesDAO extends BaseDAO<VehicleServices> {

    public List<VehicleServices> getAllForVehicle(Vehicle v) {
        List<VehicleServices> results;
        results = entityManager.createNamedQuery("VehicleServices.getByVehicle", VehicleServices.class)
                .setParameter("vehicle", v)
                .getResultList();
        return results;
    }

    @Override
    public List<VehicleServices> getAll() {
        return null;
    }

    @Override
    public Optional<VehicleServices> getById(Long id) {
        //VehicleServices.getById
        return Optional.ofNullable(entityManager.createNamedQuery("VehicleServices.getById",
                VehicleServices.class)
                .setParameter("id", id)
                .getSingleResult()
        );
    }

    @Override
    public void delete(VehicleServices v){
        entityManager.remove(v);
    }
}
