package com.nickross.autoservicemanager.services;

import com.nickross.autoservicemanager.exceptions.ValidationException;
import com.nickross.autoservicemanager.services.model.ServiceDAO;
import com.nickross.autoservicemanager.services.model.ServiceDTO;
import org.springframework.beans.factory.annotation.Autowired;

import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

/**
 * @author Nick Ross on 2018-08-20.
 */
//Here we are, we've made it - the best named Services in existence
@org.springframework.stereotype.Service
@Transactional
public class ServicesService {
    @Autowired
    private ServiceDAO serviceDAO;

    public List<ServiceDTO> getAll(){
        List<ServiceDTO> results = new ArrayList<>();
        List<com.nickross.autoservicemanager.services.model.Service> services = serviceDAO.getAll();
        services.forEach(s -> results.add(ServiceHelper.serviceEntityToDTO(s)));
        return results;
    }

    public com.nickross.autoservicemanager.services.model.Service validateService(long id) throws ValidationException {
        Optional<com.nickross.autoservicemanager.services.model.Service> service = serviceDAO.getById(id);
        List<String> errors = new ArrayList<>();
        if(!service.isPresent()){
            errors.add("Invalid service");
            throw new ValidationException(errors);
        }
        return service.get();
    }
}
