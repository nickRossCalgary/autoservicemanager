package com.nickross.autoservicemanager.services.model;

import com.nickross.autoservicemanager.data.BaseDAO;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

/**
 * @author Nick Ross on 2018-08-20.
 */
@Repository
public class ServiceDAO extends BaseDAO<Service> {
    @Override
    public List<Service> getAll() {
        List<Service> results;
        results = entityManager.createNamedQuery("Service.getAll", Service.class).getResultList();
        return results;
    }

    @Override
    public Optional<Service> getById(Long id) {
        return Optional.ofNullable(entityManager.find(Service.class, id));
    }

    @Override
    public void delete(Service s) {
        //no-op
        //TODO: communicate to consumers that this is not implemented.
    }

    @Override
    public void update(Service s) {
        //no-op
        //TODO: communicate to consumers that this is not implemented.
    }
}
