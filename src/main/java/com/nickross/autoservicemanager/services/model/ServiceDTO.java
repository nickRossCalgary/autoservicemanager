package com.nickross.autoservicemanager.services.model;

import javafx.util.converter.LocalDateStringConverter;

import java.time.LocalDateTime;

/**
 * @author Nick Ross on 2018-08-20.
 */
public class ServiceDTO {
    private String title;
    private Long id;
    private String date;
    private boolean isGasSpecific;
    private boolean isDieselSpecific;
    private boolean isElectricSpecific;


    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public boolean isGasSpecific() {
        return isGasSpecific;
    }

    public void setGasSpecific(boolean gasSpecific) {
        isGasSpecific = gasSpecific;
    }

    public boolean isDieselSpecific() {
        return isDieselSpecific;
    }

    public void setDieselSpecific(boolean dieselSpecific) {
        isDieselSpecific = dieselSpecific;
    }

    public boolean isElectricSpecific() {
        return isElectricSpecific;
    }

    public void setElectricSpecific(boolean electricSpecific) {
        isElectricSpecific = electricSpecific;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }
}
