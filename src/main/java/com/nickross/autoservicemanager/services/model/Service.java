package com.nickross.autoservicemanager.services.model;

import javax.persistence.*;

/**
 * @author Nick Ross on 2018-08-20.
 */
@Entity
@Table(name="services")
@NamedQueries({
        @NamedQuery(name="Service.getAll", query = "SELECT s FROM Service s")
})
public class Service {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Column(name = "title")
    private String title;

    @Column(name = "gas_specific")
    private Boolean gasSpecific;

    @Column(name = "electric_specific")
    private Boolean electricSpecific;

    @Column(name = "diesel_specific")
    private Boolean dieselSpecific;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Boolean getGasSpecific() {
        return gasSpecific;
    }

    public void setGasSpecific(Boolean gasSpecific) {
        this.gasSpecific = gasSpecific;
    }

    public Boolean getElectricSpecific() {
        return electricSpecific;
    }

    public void setElectricSpecific(Boolean electricSpecific) {
        this.electricSpecific = electricSpecific;
    }

    public Boolean getDieselSpecific() {
        return dieselSpecific;
    }

    public void setDieselSpecific(Boolean dieselSpecific) {
        this.dieselSpecific = dieselSpecific;
    }
}
