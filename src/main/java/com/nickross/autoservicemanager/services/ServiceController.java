package com.nickross.autoservicemanager.services;

import com.nickross.autoservicemanager.data.UrlResourcePatterns;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author Nick Ross on 2018-08-20.
 */
@CrossOrigin(origins = {"http://localhost:3000"})
@RestController
@EnableAutoConfiguration
@RequestMapping(value = UrlResourcePatterns.SERVICES)
public class ServiceController {
    @Autowired
    private ServicesService servicesService;

    @GetMapping("")
    public ResponseEntity<Object> getServices() {
        return ResponseEntity.ok().body(servicesService.getAll());
    }
}
