package com.nickross.autoservicemanager.services;

import com.nickross.autoservicemanager.services.model.Service;
import com.nickross.autoservicemanager.services.model.ServiceDTO;

/**
 * @author Nick Ross on 2018-08-20.
 */
public class ServiceHelper {

    static ServiceDTO serviceEntityToDTO(Service s){
        ServiceDTO result = new ServiceDTO();
        result.setId(s.getId());
        result.setTitle(s.getTitle());
        result.setGasSpecific(s.getGasSpecific());
        result.setDieselSpecific(s.getDieselSpecific());
        result.setElectricSpecific(s.getElectricSpecific());
        return result;
    }

}
