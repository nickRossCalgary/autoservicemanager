package com.nickross.autoservicemanager.customer.model;

import com.nickross.autoservicemanager.data.BaseDAO;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

/**
 * @author Nick Ross on 2018-08-18.
 */
@Repository
public class CustomerDAO extends BaseDAO<Customer> {

    @Override
    public List<Customer> getAll() {
        List<Customer> results;
        results = entityManager.createNamedQuery("Customer.getAll", Customer.class).getResultList();
        return results;
    }

    @Override
    public Optional<Customer> getById(Long id) {
        return Optional.ofNullable(entityManager.find(Customer.class, id));
    }
}
