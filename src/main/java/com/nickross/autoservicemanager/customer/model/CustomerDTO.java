package com.nickross.autoservicemanager.customer.model;

/**
 * @author Nick Ross on 2018-08-18.
 */
public class CustomerDTO {
    private Long id;
    private String firstName;
    private String lastName;
//    private List<VehicleDTO> vehicles;
    //TODO: Add vehicles

    public CustomerDTO(){}

    //Want to enforce the fact that customer needs a full name. TODO: actual validation? keep dto anemic
    public CustomerDTO(String firstName, String lastName) {
        this.firstName = firstName;
        this.lastName = lastName;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }
}
