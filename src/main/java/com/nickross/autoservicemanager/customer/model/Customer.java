package com.nickross.autoservicemanager.customer.model;

import javax.persistence.*;

/**
 * @author Nick Ross on 2018-08-18.
 */
@Entity
@Table(name="customers")
@NamedQueries({
        @NamedQuery(name="Customer.getAll", query = "SELECT a FROM Customer a")
})
public class Customer {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Column(name = "first_name")
    private String firstName;

    @Column(name = "last_name")
    private String lastName;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }
}
