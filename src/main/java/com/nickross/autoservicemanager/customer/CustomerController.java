package com.nickross.autoservicemanager.customer;

import com.nickross.autoservicemanager.BaseRestController;
import com.nickross.autoservicemanager.customer.model.CustomerDTO;
import com.nickross.autoservicemanager.data.UrlResourcePatterns;
import com.nickross.autoservicemanager.exceptions.ValidationException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Optional;

/**
 * @author Nick Ross on 2018-08-18.
 */
@CrossOrigin(origins = {"http://localhost:3000"})
@RestController
@EnableAutoConfiguration
@RequestMapping(value = UrlResourcePatterns.CUSTOMERS)
public class CustomerController extends BaseRestController {

    @Autowired
    private CustomerService customerService;

    @GetMapping("")
    public ResponseEntity<Object> getCustomer() {
        return ResponseEntity.ok().body(customerService.getAll());
    }

    @GetMapping("{id}")
    public ResponseEntity<Object> getCustomerById(@PathVariable("id") long id) {
        Optional<CustomerDTO> result = customerService.getById(id);
        if (result.isPresent()) {
            return ResponseEntity.ok().body(result.get());
        } else {
            return ResponseEntity.notFound().build();
        }
    }

    @PostMapping("")
    public ResponseEntity<Object> createNewCustomer(@RequestBody CustomerDTO customerDTO) {
        try {
            return ResponseEntity.ok().body(customerService.addCustomer(customerDTO));
        } catch (ValidationException e) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(e.getErrors());
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
        }
    }
}
