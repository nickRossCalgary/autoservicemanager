package com.nickross.autoservicemanager.customer;

import com.nickross.autoservicemanager.customer.model.CustomerDTO;
import com.nickross.autoservicemanager.exceptions.ValidationException;
import org.springframework.util.StringUtils;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Nick Ross on 2018-08-21.
 */
public class CustomerHelper {

    static void validate(CustomerDTO customerDTO) throws ValidationException {
        List<String> errors = new ArrayList<>();
        if(StringUtils.isEmpty(customerDTO.getFirstName())){
            errors.add("first name is missing");
        }if(StringUtils.isEmpty(customerDTO.getLastName())){
            errors.add("last name is missing");
        }

        if(!errors.isEmpty()){
            throw new ValidationException(errors);
        }
    }
}
