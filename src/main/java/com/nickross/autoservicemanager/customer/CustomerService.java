package com.nickross.autoservicemanager.customer;

import com.nickross.autoservicemanager.customer.model.Customer;
import com.nickross.autoservicemanager.customer.model.CustomerDAO;
import com.nickross.autoservicemanager.customer.model.CustomerDTO;
import com.nickross.autoservicemanager.exceptions.ValidationException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

/**
 * @author Nick Ross on 2018-08-18.
 */
@Transactional
@Service
public class CustomerService {

    @Autowired
    private CustomerDAO customerDAO;

    public Optional<CustomerDTO> getById(Long id) {
        Optional<Customer> result = customerDAO.getById(id);

        return result.map(this::entityToDto);
    }

    public List<CustomerDTO> getAll() {
        ArrayList<CustomerDTO> results = new ArrayList<>();
        for (Customer entity : customerDAO.getAll()) {
            results.add(entityToDto(entity));
        }
        return results;
    }

    public Customer validateOwner(Long ownerId) throws ValidationException {
        List<String> errors = new ArrayList<>();
        Optional<Customer> customer = customerDAO.getById(ownerId);
        if (!customer.isPresent()) {
            errors.add("Owner not found");
        }

        if (!errors.isEmpty()) {
            throw new ValidationException(errors);
        }
        return customer.get();
    }

    @Transactional
    public CustomerDTO addCustomer(CustomerDTO newCustomer) throws ValidationException {
        CustomerHelper.validate(newCustomer);
        Customer c = customerDAO.create(dtoToEntity(newCustomer));
        return entityToDto(c);
    }

    private CustomerDTO entityToDto(Customer customer) {
        CustomerDTO customerDTO = new CustomerDTO();
        customerDTO.setId(customer.getId());
        customerDTO.setFirstName(customer.getFirstName());
        customerDTO.setLastName(customer.getLastName());

        return customerDTO;
    }

    private Customer dtoToEntity(CustomerDTO customerDto) {
        Customer customer = new Customer();
        customer.setId(customerDto.getId());
        customer.setFirstName(customerDto.getFirstName());
        customer.setLastName(customerDto.getLastName());

        return customer;
    }
}
