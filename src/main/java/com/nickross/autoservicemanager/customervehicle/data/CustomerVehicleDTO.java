package com.nickross.autoservicemanager.customervehicle.data;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.nickross.autoservicemanager.vehicle.diesel.model.DieselVehicleDTO;
import com.nickross.autoservicemanager.vehicle.electric.model.ElectricVehicleDTO;
import com.nickross.autoservicemanager.vehicle.gas.model.GasVehicleDTO;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Nick Ross on 2018-08-19.
 */
public class CustomerVehicleDTO {
    private Long customerId;
    @JsonProperty("electricVehicles")
    private List<ElectricVehicleDTO> electricVehicleDTOS = new ArrayList<>();
    @JsonProperty("dieselVehicles")
    private List<DieselVehicleDTO> dieselVehicleDTOS = new ArrayList<>();
    @JsonProperty("gasVehicles")
    private List<GasVehicleDTO> gasVehicleDTOS = new ArrayList<>();

    public Long getCustomerId() {
        return customerId;
    }

    public void setCustomerId(Long customerId) {
        this.customerId = customerId;
    }

    public List<ElectricVehicleDTO> getElectricVehicleDTOS() {
        return electricVehicleDTOS;
    }

    public void setElectricVehicleDTOS(List<ElectricVehicleDTO> electricVehicleDTOS) {
        this.electricVehicleDTOS = electricVehicleDTOS;
    }

    public List<DieselVehicleDTO> getDieselVehicleDTOS() {
        return dieselVehicleDTOS;
    }

    public void setDieselVehicleDTOS(List<DieselVehicleDTO> dieselVehicleDTOS) {
        this.dieselVehicleDTOS = dieselVehicleDTOS;
    }

    public List<GasVehicleDTO> getGasVehicleDTOS() {
        return gasVehicleDTOS;
    }

    public void setGasVehicleDTOS(List<GasVehicleDTO> gasVehicleDTOS) {
        this.gasVehicleDTOS = gasVehicleDTOS;
    }
}
