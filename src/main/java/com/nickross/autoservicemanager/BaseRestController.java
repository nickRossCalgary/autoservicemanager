package com.nickross.autoservicemanager;

import org.springframework.http.HttpStatus;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.web.bind.MissingServletRequestParameterException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashMap;
import java.util.Map;

/**
 * @author Nick Ross on 2018-08-18.
 */
@RestController
public class BaseRestController {
    @ExceptionHandler
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    public Map<String, Object> handleMissingParam(MissingServletRequestParameterException e) {
        Map<String, Object> result = new HashMap<>();

        result.put("status", Integer.toString(400));

        Map<String, String> error = new HashMap<>();
        error.put("param", e.getParameterName());

        result.put("error", error);
        return result;
    }

    @ExceptionHandler
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    public Map<String, Object> handleMissingParam(HttpMessageNotReadableException e) {
        Map<String, Object> result = new HashMap<>();
        result.put("status", Integer.toString(400));

        result.put("error", "missformed request");
        return result;
    }
}
