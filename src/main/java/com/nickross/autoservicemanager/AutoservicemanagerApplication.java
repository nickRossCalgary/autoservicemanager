package com.nickross.autoservicemanager;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class AutoservicemanagerApplication {

	public static void main(String[] args) {
		SpringApplication.run(AutoservicemanagerApplication.class, args);
	}
}
