package com.nickross.autoservicemanager.data;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.List;
import java.util.Optional;

/**
 * @author Nick Ross on 2018-08-18.
 */
public abstract class BaseDAO<Entity> {
    @PersistenceContext
    protected EntityManager entityManager;

    public abstract List<Entity> getAll();

    public abstract Optional<Entity> getById(Long id);

    public Entity create(Entity e) {
        entityManager.persist(e);
        return e;
    }

    public void update(Entity e) {
        entityManager.merge(e);
    }

    public void delete(Entity e) {
        entityManager.remove(e);
    }//todo: nullchecks
}
