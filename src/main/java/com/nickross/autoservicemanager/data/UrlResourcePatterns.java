package com.nickross.autoservicemanager.data;

/**
 * @author Nick Ross on 2018-08-18.
 */
public class UrlResourcePatterns {
    public static final String CUSTOMERS = "customers";
    public static final String VEHICLES = "vehicles";
    public static final String SERVICES =  "services";
}
