package com.nickross.autoservicemanager.exceptions;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Nick Ross on 2018-08-18.
 */
public class ValidationException extends Exception {
    private List<String> errors = new ArrayList<>();
    public ValidationException(List<String> errors){
//        super(msg);
        this.errors = errors;
    }

    public List<String> getErrors() {
        return errors;
    }
}
