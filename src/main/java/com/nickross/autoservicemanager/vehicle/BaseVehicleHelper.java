package com.nickross.autoservicemanager.vehicle;

import com.nickross.autoservicemanager.exceptions.ValidationException;
import com.nickross.autoservicemanager.vehicle.model.BaseVehicleDTO;
import com.nickross.autoservicemanager.vehicle.model.Vehicle;
import org.springframework.util.StringUtils;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Nick Ross on 2018-08-18.
 */
public class BaseVehicleHelper {

    public static void validate(BaseVehicleDTO vehicleDTO) throws ValidationException {
        List<String> errors = new ArrayList<>();
        if (StringUtils.isEmpty(vehicleDTO.getMake())) {
            errors.add("Missing make");
        }
        if (StringUtils.isEmpty(vehicleDTO.getModel())) {
            errors.add("Missing model");
        }
        if (vehicleDTO.getYear() == 0) {
            errors.add("Missing year");
        }
        if (vehicleDTO.getOdometer() == 0) {
            errors.add("Missing odometer");
        }

        if (!errors.isEmpty()) {
            throw new ValidationException(errors);
        }
    }

    public static Vehicle extractBaseVehicle(BaseVehicleDTO vehicleDTO) {
        Vehicle baseVehicle = new Vehicle();
        baseVehicle.setMake(vehicleDTO.getMake());
        baseVehicle.setModel(vehicleDTO.getModel());
        baseVehicle.setYear(vehicleDTO.getYear());
        baseVehicle.setOdometer(vehicleDTO.getOdometer());

        return baseVehicle;
    }

}
