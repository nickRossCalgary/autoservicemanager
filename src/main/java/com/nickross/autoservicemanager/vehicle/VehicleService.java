package com.nickross.autoservicemanager.vehicle;

import com.nickross.autoservicemanager.exceptions.ValidationException;
import com.nickross.autoservicemanager.vehicle.diesel.DieselVehicleService;
import com.nickross.autoservicemanager.vehicle.diesel.model.DieselVehicle;
import com.nickross.autoservicemanager.vehicle.diesel.model.DieselVehicleDTO;
import com.nickross.autoservicemanager.vehicle.electric.ElectricVehicleService;
import com.nickross.autoservicemanager.vehicle.electric.model.ElectricVehicle;
import com.nickross.autoservicemanager.vehicle.electric.model.ElectricVehicleDTO;
import com.nickross.autoservicemanager.vehicle.gas.GasVehicleService;
import com.nickross.autoservicemanager.vehicle.gas.model.GasVehicle;
import com.nickross.autoservicemanager.vehicle.gas.model.GasVehicleDTO;
import com.nickross.autoservicemanager.customer.CustomerService;
import com.nickross.autoservicemanager.customer.model.Customer;
import com.nickross.autoservicemanager.customervehicle.data.CustomerVehicleDTO;
import com.nickross.autoservicemanager.vehicle.model.BaseVehicleDTO;
import com.nickross.autoservicemanager.vehicle.model.Vehicle;
import com.nickross.autoservicemanager.vehicle.model.VehicleDAO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

/**
 * @author Nick Ross on 2018-08-18.
 */
@Transactional
@Service
public class VehicleService {

    @Autowired
    private ElectricVehicleService electricVehicleService;
    @Autowired
    private DieselVehicleService dieselVehicleService;
    @Autowired
    private GasVehicleService gasVehicleService;
    @Autowired
    private CustomerService customerService;
    @Autowired
    private VehicleDAO vehicleDAO;


    public CustomerVehicleDTO getCustomerVehicles(Long customerId) throws ValidationException {
        Customer owner = customerService.validateOwner(customerId);
        CustomerVehicleDTO results = new CustomerVehicleDTO();
        results.setCustomerId(owner.getId());

        results.getElectricVehicleDTOS().addAll(electricVehicleService.getByCustomerId(owner));
        results.getDieselVehicleDTOS().addAll(dieselVehicleService.getByCustomerId(owner));
        results.getGasVehicleDTOS().addAll(gasVehicleService.getByCustomerId(owner));

        return results;
    }

    @Transactional
    public void addVehicle(long customerId, BaseVehicleDTO vehicleDTO) throws ValidationException {
        if (vehicleDTO instanceof ElectricVehicleDTO) {
            electricVehicleService.addElectricVehicle(customerId, (ElectricVehicleDTO) vehicleDTO);
        } else if (vehicleDTO instanceof GasVehicleDTO) {
            gasVehicleService.addGasVehicle(customerId, (GasVehicleDTO) vehicleDTO);
        } else if (vehicleDTO instanceof DieselVehicleDTO) {
            dieselVehicleService.addDieselVehicle(customerId, (DieselVehicleDTO) vehicleDTO);
        } else {
            List<String> errors = new ArrayList<>();
            errors.add("Unsupported vehicle");
            throw new ValidationException(errors);
        }

    }

    public BaseVehicleDTO getTypedVehicleByVehicle(Vehicle v) throws ValidationException {
        Optional<ElectricVehicleDTO> e = electricVehicleService.getByVehicle(v);
        if (e.isPresent()) {
            return e.get();
        }
        Optional<DieselVehicleDTO> d = dieselVehicleService.getByVehicle(v);
        if (d.isPresent()) {
            return d.get();
        }
        Optional<GasVehicleDTO> g = gasVehicleService.getByVehicle(v);
        if (g.isPresent()) {
            return g.get();
        }
        List<String> errors = new ArrayList<>();
        errors.add("Vehicle not found");
        throw new ValidationException(errors);
    }

    public Vehicle validateVehicle(long id) throws ValidationException {
        List<String> errors = new ArrayList<>();
        Optional<Vehicle> vehicle = vehicleDAO.getById(id);
        if (!vehicle.isPresent()) {
            errors.add("Missing vehicle");
            throw new ValidationException(errors);
        }
        return vehicle.get();
    }

    @Transactional
    public void removeVehicle(Long id) throws ValidationException {
        Optional<Vehicle> vehicle = vehicleDAO.getById(id);
        if (!vehicle.isPresent()) {
            List<String> errors = new ArrayList<>();
            errors.add("Vehicle not found");
            throw new ValidationException(errors);
        }

        Optional<ElectricVehicle> e = electricVehicleService.getByVehicleRaw(vehicle.get());
        e.ifPresent(electricVehicle -> {
            electricVehicleService.removeVehicle(electricVehicle);
            return;
        });
        Optional<DieselVehicle> d = dieselVehicleService.getByVehicleRaw(vehicle.get());
        d.ifPresent(dieselVehicle -> {
            dieselVehicleService.removeVehicle(dieselVehicle);
            return;
        });
        Optional<GasVehicle> g = gasVehicleService.getByVehicleRaw(vehicle.get());
        g.ifPresent(gasVehicle -> {
            gasVehicleService.removeVehicle(gasVehicle);
            return;
        });
    }
}
