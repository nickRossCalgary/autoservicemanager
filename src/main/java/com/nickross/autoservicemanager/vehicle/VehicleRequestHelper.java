package com.nickross.autoservicemanager.vehicle;

import com.nickross.autoservicemanager.exceptions.ValidationException;
import com.nickross.autoservicemanager.vehicle.diesel.model.DieselVehicleDTO;
import com.nickross.autoservicemanager.vehicle.electric.model.ElectricVehicleDTO;
import com.nickross.autoservicemanager.vehicle.gas.model.GasVehicleDTO;
import com.nickross.autoservicemanager.vehicle.model.BaseVehicleDTO;
import com.nickross.autoservicemanager.vehicle.model.RequestVehicleDTO;
import org.springframework.util.StringUtils;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Nick Ross on 2018-08-21.
 */
class VehicleRequestHelper {

    static BaseVehicleDTO parseRequestVehicleToTypedVehicle(RequestVehicleDTO requestVehicleDTO)
            throws ValidationException {
        if(!StringUtils.isEmpty(requestVehicleDTO.getType())){
            if(requestVehicleDTO.getType().equals("electric")
                    && requestVehicleDTO.getBatteryCapacity() > 0){
                ElectricVehicleDTO electricVehicleDTO = new ElectricVehicleDTO();
                electricVehicleDTO.setBatteryCapacity(requestVehicleDTO.getBatteryCapacity());
                setBaseValues(electricVehicleDTO, requestVehicleDTO);
                return electricVehicleDTO;
            }else if(requestVehicleDTO.getType().equals("gas")
                    && !StringUtils.isEmpty(requestVehicleDTO.getOilType())){
                GasVehicleDTO gasVehicleDTO = new GasVehicleDTO();
                gasVehicleDTO.setOilType(requestVehicleDTO.getOilType());
                setBaseValues(gasVehicleDTO, requestVehicleDTO);
                return gasVehicleDTO;
            }else if(requestVehicleDTO.getType().equals("diesel")
                    && !StringUtils.isEmpty(requestVehicleDTO.getDieselFilter())){
                DieselVehicleDTO dieselVehicleDTO = new DieselVehicleDTO();
                dieselVehicleDTO.setDiselFilter(requestVehicleDTO.getDieselFilter());
                setBaseValues(dieselVehicleDTO, requestVehicleDTO);
                return dieselVehicleDTO;
            }
        }
        List<String> errors = new ArrayList();
        errors.add("Invalid vehicle");

        throw new ValidationException(errors);
    }

    private static void setBaseValues(BaseVehicleDTO vehicleDTO, RequestVehicleDTO requestVehicleDTO) {
        vehicleDTO.setMake(requestVehicleDTO.getMake());
        vehicleDTO.setModel(requestVehicleDTO.getModel());
        vehicleDTO.setYear(requestVehicleDTO.getYear());
        vehicleDTO.setOdometer(requestVehicleDTO.getOdometer());
    }
}
