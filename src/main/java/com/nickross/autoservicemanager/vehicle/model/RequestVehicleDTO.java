package com.nickross.autoservicemanager.vehicle.model;

/**
 * @author Nick Ross on 2018-08-21.
 */
public class RequestVehicleDTO {
    private String make;
    private String model;
    private int year;
    private long odometer;
    private long ownerId;
    private String oilType;
    private int batteryCapacity;
    private String dieselFilter;
    private String type;

    public String getMake() {
        return make;
    }

    public void setMake(String make) {
        this.make = make;
    }

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public int getYear() {
        return year;
    }

    public void setYear(int year) {
        this.year = year;
    }

    public long getOdometer() {
        return odometer;
    }

    public void setOdometer(long odometer) {
        this.odometer = odometer;
    }

    public long getOwnerId() {
        return ownerId;
    }

    public void setOwnerId(long ownerId) {
        this.ownerId = ownerId;
    }

    public String getOilType() {
        return oilType;
    }

    public void setOilType(String oilType) {
        this.oilType = oilType;
    }

    public int getBatteryCapacity() {
        return batteryCapacity;
    }

    public void setBatteryCapacity(int batteryCapacity) {
        this.batteryCapacity = batteryCapacity;
    }

    public String getDieselFilter() {
        return dieselFilter;
    }

    public void setDieselFilter(String dieselFilter) {
        this.dieselFilter = dieselFilter;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }
}
