package com.nickross.autoservicemanager.vehicle.model;

import com.nickross.autoservicemanager.data.BaseDAO;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

/**
 * @author Nick Ross on 2018-08-18.
 */
@Repository
public class VehicleDAO extends BaseDAO<Vehicle> {

    @Override
    public void delete(Vehicle c) {
        entityManager.remove(c);
    }

    @Override
    public List<Vehicle> getAll() {
        List<Vehicle> results = entityManager.createNamedQuery("Vehicle.getAll", Vehicle.class).getResultList();
        return results;
    }

    @Override
    public Optional<Vehicle> getById(Long id) {
        return Optional.ofNullable(entityManager.find(Vehicle.class, id));
    }
}
