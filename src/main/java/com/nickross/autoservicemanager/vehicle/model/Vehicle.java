package com.nickross.autoservicemanager.vehicle.model;

import javax.persistence.*;

/**
 * @author Nick Ross on 2018-08-18.
 */
@Entity
@Table(name="vehicles")
@NamedQueries({
        @NamedQuery(name="Vehicle.getAll", query = "SELECT a FROM Vehicle a")
})
public class Vehicle {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Column(name = "make")
    private String make;

    @Column(name = "model")
    private String model;

    @Column(name = "year")
    private int year;

    @Column(name = "odometer")
    private long odometer;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getMake() {
        return make;
    }

    public void setMake(String make) {
        this.make = make;
    }

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public int getYear() {
        return year;
    }

    public void setYear(int year) {
        this.year = year;
    }

    public long getOdometer() {
        return odometer;
    }

    public void setOdometer(long odometer) {
        this.odometer = odometer;
    }
}
