package com.nickross.autoservicemanager.vehicle.model;

/**
 * @author Nick Ross on 2018-08-18.
 */
public abstract class BaseVehicleDTO {
    private Long baseId;
    private String make;
    private String model;
    private int year;
    private long odometer;
    private long ownerId;

    public Long getBaseId() {
        return baseId;
    }

    public void setBaseId(Long id) {
        this.baseId = id;
    }

    public String getMake() {
        return make;
    }

    public void setMake(String make) {
        this.make = make;
    }

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public int getYear() {
        return year;
    }

    public void setYear(int year) {
        this.year = year;
    }

    public long getOdometer() {
        return odometer;
    }

    public void setOdometer(long odometer) {
        this.odometer = odometer;
    }

    public long getOwnerId() {
        return ownerId;
    }

    public void setOwnerId(long ownerId) {
        this.ownerId = ownerId;
    }
}
