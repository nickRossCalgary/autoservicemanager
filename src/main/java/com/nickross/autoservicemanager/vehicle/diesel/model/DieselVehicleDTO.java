package com.nickross.autoservicemanager.vehicle.diesel.model;

import com.nickross.autoservicemanager.vehicle.model.BaseVehicleDTO;

/**
 * @author Nick Ross on 2018-08-18.
 */
public class DieselVehicleDTO extends BaseVehicleDTO {
    private Long id;
    private String diselFilter;

    public DieselVehicleDTO() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getDiselFilter() {
        return diselFilter;
    }

    public void setDiselFilter(String diselFilter) {
        this.diselFilter = diselFilter;
    }
}
