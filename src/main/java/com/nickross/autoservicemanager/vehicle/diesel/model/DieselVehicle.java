package com.nickross.autoservicemanager.vehicle.diesel.model;

import com.nickross.autoservicemanager.vehicle.model.Vehicle;
import com.nickross.autoservicemanager.customer.model.Customer;

import javax.persistence.*;

/**
 * @author Nick Ross on 2018-08-18.
 */
@Entity
@Table(name="diesel_vehicles")
@NamedQueries({
        @NamedQuery(name="DieselVehicle.getAll", query = "SELECT a FROM DieselVehicle a"),
        @NamedQuery(name="DieselVehicle.getByCustomerId", query = "SELECT a FROM DieselVehicle a WHERE a.owner = " +
                ":owner"),
        @NamedQuery(name="DieselVehicle.getByVehicle",
                query = "SELECT a FROM DieselVehicle a WHERE a.baseVehicle = :vehicle")
})
public class DieselVehicle {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @JoinColumn
    @OneToOne(cascade=CascadeType.REMOVE)
    private Vehicle baseVehicle;

    @Column(name = "diesel_filter")
    private String dieselFilter;

    @ManyToOne
    @JoinColumn(name = "owner_id")
    private Customer owner;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Vehicle getBaseVehicle() {
        return baseVehicle;
    }

    public void setBaseVehicle(Vehicle baseVehicle) {
        this.baseVehicle = baseVehicle;
    }

    public String getDieselFilter() {
        return dieselFilter;
    }

    public void setDieselFilter(String dieselFilter) {
        this.dieselFilter = dieselFilter;
    }

    public Customer getOwner() {
        return owner;
    }

    public void setOwner(Customer owner) {
        this.owner = owner;
    }
}
