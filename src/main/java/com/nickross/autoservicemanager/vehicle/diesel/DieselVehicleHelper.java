package com.nickross.autoservicemanager.vehicle.diesel;

import com.nickross.autoservicemanager.exceptions.ValidationException;
import com.nickross.autoservicemanager.vehicle.BaseVehicleHelper;
import com.nickross.autoservicemanager.vehicle.diesel.model.DieselVehicle;
import com.nickross.autoservicemanager.vehicle.diesel.model.DieselVehicleDTO;
import org.springframework.util.StringUtils;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Nick Ross on 2018-08-18.
 */
class DieselVehicleHelper {

    static void validate(DieselVehicleDTO dieselVehicleDTO) throws ValidationException {
        List<String> errors = new ArrayList<>();
        BaseVehicleHelper.validate(dieselVehicleDTO);
        if (StringUtils.isEmpty(dieselVehicleDTO.getDiselFilter())) {
            errors.add("Missing diesel filter");
        }

        if(!errors.isEmpty()){
            throw new ValidationException(errors);
        }
    }

    static DieselVehicleDTO dieselVehicleEntityToDto(DieselVehicle v){
        DieselVehicleDTO result = new DieselVehicleDTO();
        result.setMake(v.getBaseVehicle().getMake());
        result.setModel(v.getBaseVehicle().getModel());
        result.setYear(v.getBaseVehicle().getYear());
        result.setOdometer(v.getBaseVehicle().getOdometer());
        result.setBaseId(v.getBaseVehicle().getId());
        result.setDiselFilter(v.getDieselFilter());
        result.setId(v.getId());

        return result;
    }
}
