package com.nickross.autoservicemanager.vehicle.diesel;

import com.nickross.autoservicemanager.customer.CustomerService;
import com.nickross.autoservicemanager.customer.model.Customer;
import com.nickross.autoservicemanager.exceptions.ValidationException;
import com.nickross.autoservicemanager.vehicle.BaseVehicleHelper;
import com.nickross.autoservicemanager.vehicle.diesel.model.DieselVehicle;
import com.nickross.autoservicemanager.vehicle.diesel.model.DieselVehicleDAO;
import com.nickross.autoservicemanager.vehicle.diesel.model.DieselVehicleDTO;
import com.nickross.autoservicemanager.vehicle.model.Vehicle;
import com.nickross.autoservicemanager.vehicle.model.VehicleDAO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

/**
 * @author Nick Ross on 2018-08-19.
 */
@Service
public class DieselVehicleService {
    @Autowired
    private DieselVehicleDAO dieselVehicleDAO;
    @Autowired
    private VehicleDAO vehicleDAO;
    @Autowired
    private CustomerService customerService;

    @Transactional
    public void addDieselVehicle(Long customerId, DieselVehicleDTO dieselVehicleDTO) throws ValidationException {
        Customer owner = customerService.validateOwner(customerId);
        DieselVehicleHelper.validate(dieselVehicleDTO);

        Vehicle vehicle = BaseVehicleHelper.extractBaseVehicle(dieselVehicleDTO);
        DieselVehicle dieselVehicle = new DieselVehicle();
        dieselVehicle.setBaseVehicle(vehicle);
        dieselVehicle.setOwner(owner);
        dieselVehicle.setDieselFilter(dieselVehicleDTO.getDiselFilter());

        vehicleDAO.create(vehicle);
        dieselVehicleDAO.create(dieselVehicle);
    }

    public List<DieselVehicleDTO> getByCustomerId(Customer owner) {
        List<DieselVehicle> diesel = dieselVehicleDAO.getByCustomerId(owner);
        List<DieselVehicleDTO> results = new ArrayList<>();
        diesel.forEach(d -> results.add(DieselVehicleHelper.dieselVehicleEntityToDto(d)));
        return results;
    }

    public Optional<DieselVehicleDTO> getByVehicle(Vehicle v) {
        try {
            return Optional.ofNullable(DieselVehicleHelper.dieselVehicleEntityToDto(dieselVehicleDAO.getByVehicle(v)));
        } catch (EmptyResultDataAccessException e) {
            return Optional.empty();
        }
    }

    public Optional<DieselVehicle> getByVehicleRaw(Vehicle v) {
        try {
            return Optional.ofNullable(dieselVehicleDAO.getByVehicle(v));
        } catch (EmptyResultDataAccessException e) {
            return Optional.empty();
        }
    }

    public void removeVehicle(DieselVehicle e) {
        dieselVehicleDAO.delete(e);
    }
}
