package com.nickross.autoservicemanager.vehicle.diesel.model;

import com.nickross.autoservicemanager.customer.model.Customer;
import com.nickross.autoservicemanager.data.BaseDAO;
import com.nickross.autoservicemanager.vehicle.model.Vehicle;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

/**
 * @author Nick Ross on 2018-08-18.
 */
@Repository
public class DieselVehicleDAO extends BaseDAO<DieselVehicle> {

    @Override
    public List<DieselVehicle> getAll() {
        List<DieselVehicle> results = entityManager
                .createNamedQuery("DieselVehicle.getAll", DieselVehicle.class)
                .getResultList();
        return results;
    }

    @Override
    public Optional<DieselVehicle> getById(Long id) {
        return Optional.ofNullable(entityManager.find(DieselVehicle.class, id));
    }

    public List<DieselVehicle> getByCustomerId(Customer owner) {
        List<DieselVehicle> results;
        results = entityManager.createNamedQuery("DieselVehicle.getByCustomerId", DieselVehicle.class)
                .setParameter("owner", owner)
                .getResultList();
        return results;
    }

    public DieselVehicle getByVehicle(Vehicle v) {
        return entityManager
                .createNamedQuery("DieselVehicle.getByVehicle", DieselVehicle.class)
                .setParameter("vehicle", v)
                .getSingleResult();
    }
}
