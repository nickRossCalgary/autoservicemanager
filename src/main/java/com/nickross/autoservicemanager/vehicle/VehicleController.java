package com.nickross.autoservicemanager.vehicle;

import com.nickross.autoservicemanager.BaseRestController;
import com.nickross.autoservicemanager.exceptions.ValidationException;
import com.nickross.autoservicemanager.vehicle.diesel.model.DieselVehicleDTO;
import com.nickross.autoservicemanager.vehicle.electric.model.ElectricVehicleDTO;
import com.nickross.autoservicemanager.vehicle.gas.model.GasVehicleDTO;
import com.nickross.autoservicemanager.customervehicle.data.CustomerVehicleDTO;
import com.nickross.autoservicemanager.data.UrlResourcePatterns;
import com.nickross.autoservicemanager.vehicle.model.BaseVehicleDTO;
import com.nickross.autoservicemanager.vehicle.model.RequestVehicleDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * @author Nick Ross on 2018-08-18.
 */
@CrossOrigin(origins = {"http://localhost:3000"})
@RestController
@EnableAutoConfiguration
@RequestMapping(value = UrlResourcePatterns.CUSTOMERS + "/{customerId}/" + UrlResourcePatterns.VEHICLES)
public class VehicleController extends BaseRestController {

    @Autowired
    private VehicleService vehicleService;

    @GetMapping("")
    public ResponseEntity<Object> getCustomerVehicles(@PathVariable("customerId") long customerId) {
        try {
            CustomerVehicleDTO result = vehicleService.getCustomerVehicles(customerId);
            return ResponseEntity.ok().body(result);
        } catch (ValidationException e) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(e.getErrors());
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
        }
    }

    @PostMapping("")
    public ResponseEntity<Object> addVehicleToCustomer(@PathVariable("customerId") long customerId,
                                                       @RequestBody RequestVehicleDTO v) {
        try {
            vehicleService.addVehicle(customerId, VehicleRequestHelper.parseRequestVehicleToTypedVehicle(v));
            return ResponseEntity.status(HttpStatus.CREATED).build();
        } catch (ValidationException e) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(e.getErrors());
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
        }
    }

}
