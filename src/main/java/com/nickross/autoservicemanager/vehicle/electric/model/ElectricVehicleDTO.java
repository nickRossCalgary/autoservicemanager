package com.nickross.autoservicemanager.vehicle.electric.model;

import com.nickross.autoservicemanager.vehicle.model.BaseVehicleDTO;

/**
 * @author Nick Ross on 2018-08-18.
 */
public class ElectricVehicleDTO extends BaseVehicleDTO {
    private Long id;
    private int batteryCapacity;

    public ElectricVehicleDTO(){}

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public int getBatteryCapacity() {
        return batteryCapacity;
    }

    public void setBatteryCapacity(int batteryCapacity) {
        this.batteryCapacity = batteryCapacity;
    }
}
