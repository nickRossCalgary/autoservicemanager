package com.nickross.autoservicemanager.vehicle.electric.model;

import com.nickross.autoservicemanager.vehicle.model.Vehicle;
import com.nickross.autoservicemanager.customer.model.Customer;

import javax.persistence.*;

/**
 * @author Nick Ross on 2018-08-18.
 */
@Entity
@Table(name="electric_vehicles")
@NamedQueries({
        @NamedQuery(name="ElectricVehicle.getAll", query = "SELECT a FROM ElectricVehicle a"),
        @NamedQuery(name="ElectricVehicle.getByCustomerId",
                query = "SELECT a FROM ElectricVehicle a WHERE a.owner = :owner"),
        @NamedQuery(name="ElectricVehicle.getByVehicle",
                query = "SELECT a FROM ElectricVehicle a WHERE a.baseVehicle = :vehicle")
})
public class ElectricVehicle {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @JoinColumn
    @OneToOne(cascade=CascadeType.REMOVE)
    private Vehicle baseVehicle;

    @Column(name = "battery_capacity")
    private int batteryCapacity;

    @ManyToOne
    @JoinColumn(name = "owner_id")
    private Customer owner;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Vehicle getBaseVehicle() {
        return baseVehicle;
    }

    public void setBaseVehicle(Vehicle baseVehicle) {
        this.baseVehicle = baseVehicle;
    }

    public int getBatteryCapacity() {
        return batteryCapacity;
    }

    public void setBatteryCapacity(int batteryCapacity) {
        this.batteryCapacity = batteryCapacity;
    }

    public Customer getOwner() {
        return owner;
    }

    public void setOwner(Customer owner) {
        this.owner = owner;
    }
}
