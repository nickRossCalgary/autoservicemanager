package com.nickross.autoservicemanager.vehicle.electric.model;

import com.nickross.autoservicemanager.customer.model.Customer;
import com.nickross.autoservicemanager.data.BaseDAO;
import com.nickross.autoservicemanager.vehicle.model.Vehicle;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

/**
 * @author Nick Ross on 2018-08-18.
 */
@Repository
public class ElectricVehicleDAO extends BaseDAO<ElectricVehicle> {

    @Override
    public List<ElectricVehicle> getAll() {
        List<ElectricVehicle> results;
        results = entityManager.createNamedQuery("ElectricVehicle.getAll", ElectricVehicle.class).getResultList();
        return results;
    }

    @Override
    public Optional<ElectricVehicle> getById(Long id) {
        return Optional.ofNullable(entityManager.find(ElectricVehicle.class, id));
    }

    public List<ElectricVehicle> getByCustomerId(Customer owner) {
        List<ElectricVehicle> results;
        results = entityManager.createNamedQuery("ElectricVehicle.getByCustomerId", ElectricVehicle.class)
                .setParameter("owner", owner)
                .getResultList();
        return results;
    }

    public ElectricVehicle getByVehicle(Vehicle v) {
        return entityManager
                .createNamedQuery("ElectricVehicle.getByVehicle", ElectricVehicle.class)
                .setParameter("vehicle", v)
                .getSingleResult();
    }

}
