package com.nickross.autoservicemanager.vehicle.electric;

import com.nickross.autoservicemanager.customer.CustomerService;
import com.nickross.autoservicemanager.customer.model.Customer;
import com.nickross.autoservicemanager.exceptions.ValidationException;
import com.nickross.autoservicemanager.vehicle.BaseVehicleHelper;
import com.nickross.autoservicemanager.vehicle.electric.model.ElectricVehicle;
import com.nickross.autoservicemanager.vehicle.electric.model.ElectricVehicleDAO;
import com.nickross.autoservicemanager.vehicle.electric.model.ElectricVehicleDTO;
import com.nickross.autoservicemanager.vehicle.model.Vehicle;
import com.nickross.autoservicemanager.vehicle.model.VehicleDAO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.stereotype.Service;

import javax.persistence.NoResultException;
import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

/**
 * @author Nick Ross on 2018-08-19.
 */
@Service
public class ElectricVehicleService {
    @Autowired
    private ElectricVehicleDAO electricVehicleDAO;
    @Autowired
    private VehicleDAO vehicleDAO;
    @Autowired
    private CustomerService customerService;

    @Transactional
    public void addElectricVehicle(Long customerId, ElectricVehicleDTO electricVehicleDTO) throws ValidationException {
        Customer owner = customerService.validateOwner(customerId);
        ElectricVehicleHelper.validate(electricVehicleDTO);

        Vehicle vehicle = BaseVehicleHelper.extractBaseVehicle(electricVehicleDTO);
        ElectricVehicle electricVehicle = new ElectricVehicle();
        electricVehicle.setBaseVehicle(vehicle);
        electricVehicle.setOwner(owner);
        electricVehicle.setBatteryCapacity(electricVehicleDTO.getBatteryCapacity());

        vehicleDAO.create(vehicle);
        electricVehicleDAO.create(electricVehicle);
    }

    public Optional<ElectricVehicleDTO> getByVehicle(Vehicle v) {
        try {
           return Optional.ofNullable(ElectricVehicleHelper.electricVehicleEntityToDto(electricVehicleDAO.getByVehicle(v)));
        } catch (EmptyResultDataAccessException e) {
           return Optional.empty();
        }
    }

    public Optional<ElectricVehicle> getByVehicleRaw(Vehicle v) {
        try {
           return Optional.ofNullable(electricVehicleDAO.getByVehicle(v));
        } catch (EmptyResultDataAccessException e) {
           return Optional.empty();
        }
    }

    public List<ElectricVehicleDTO> getByCustomerId(Customer owner) {
        List<ElectricVehicle> electric = electricVehicleDAO.getByCustomerId(owner);
        List<ElectricVehicleDTO> results = new ArrayList<>();
        electric.forEach(e -> results.add(ElectricVehicleHelper.electricVehicleEntityToDto(e)));
        return results;
    }

    public void removeVehicle(ElectricVehicle e) {
        electricVehicleDAO.delete(e);
    }
}
