package com.nickross.autoservicemanager.vehicle.electric;

import com.nickross.autoservicemanager.exceptions.ValidationException;
import com.nickross.autoservicemanager.vehicle.BaseVehicleHelper;
import com.nickross.autoservicemanager.vehicle.electric.model.ElectricVehicle;
import com.nickross.autoservicemanager.vehicle.electric.model.ElectricVehicleDTO;
import org.springframework.util.StringUtils;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Nick Ross on 2018-08-18.
 */
class ElectricVehicleHelper {

    static void validate(ElectricVehicleDTO electricVehicleDTO) throws ValidationException {
        List<String> errors = new ArrayList<>();
        BaseVehicleHelper.validate(electricVehicleDTO);
        if (StringUtils.isEmpty(electricVehicleDTO.getBatteryCapacity())) {
            errors.add("Missing battery capacity");
        }

        if(!errors.isEmpty()){
            throw new ValidationException(errors);
        }
    }

    static ElectricVehicleDTO electricVehicleEntityToDto(ElectricVehicle v){
        ElectricVehicleDTO result = new ElectricVehicleDTO();
        result.setMake(v.getBaseVehicle().getMake());
        result.setModel(v.getBaseVehicle().getModel());
        result.setYear(v.getBaseVehicle().getYear());
        result.setOdometer(v.getBaseVehicle().getOdometer());
        result.setBaseId(v.getBaseVehicle().getId());
        result.setBatteryCapacity(v.getBatteryCapacity());
        result.setId(v.getId());

        return result;
    }
}
