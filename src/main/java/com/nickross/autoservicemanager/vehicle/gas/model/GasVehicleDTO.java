package com.nickross.autoservicemanager.vehicle.gas.model;

import com.nickross.autoservicemanager.vehicle.model.BaseVehicleDTO;

/**
 * @author Nick Ross on 2018-08-18.
 */
public class GasVehicleDTO extends BaseVehicleDTO {
    private Long id;
    private String oilType;

    public GasVehicleDTO() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getOilType() {
        return oilType;
    }

    public void setOilType(String oilType) {
        this.oilType = oilType;
    }
}
