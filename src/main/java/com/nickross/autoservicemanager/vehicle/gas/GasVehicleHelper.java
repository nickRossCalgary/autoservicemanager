package com.nickross.autoservicemanager.vehicle.gas;

import com.nickross.autoservicemanager.exceptions.ValidationException;
import com.nickross.autoservicemanager.vehicle.BaseVehicleHelper;
import com.nickross.autoservicemanager.vehicle.gas.model.GasVehicle;
import com.nickross.autoservicemanager.vehicle.gas.model.GasVehicleDTO;
import org.springframework.util.StringUtils;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Nick Ross on 2018-08-18.
 */
class GasVehicleHelper {

    static void validate(GasVehicleDTO gasVehicleDTO) throws ValidationException {
        List<String> errors = new ArrayList<>();
        BaseVehicleHelper.validate(gasVehicleDTO);
        if (StringUtils.isEmpty(gasVehicleDTO.getOilType())) {
            errors.add("Missing oil type");
        }

        if(!errors.isEmpty()){
            throw new ValidationException(errors);
        }
    }

    static GasVehicleDTO gasVehicleEntityToDto(GasVehicle v){
        GasVehicleDTO result = new GasVehicleDTO();
        result.setMake(v.getBaseVehicle().getMake());
        result.setModel(v.getBaseVehicle().getModel());
        result.setYear(v.getBaseVehicle().getYear());
        result.setOdometer(v.getBaseVehicle().getOdometer());
        result.setBaseId(v.getBaseVehicle().getId());
        result.setOilType(v.getOilType());
        result.setId(v.getId());

        return result;
    }
}
