package com.nickross.autoservicemanager.vehicle.gas.model;

import com.nickross.autoservicemanager.vehicle.model.Vehicle;
import com.nickross.autoservicemanager.customer.model.Customer;
import org.hibernate.annotations.Cascade;

import javax.persistence.*;

/**
 * @author Nick Ross on 2018-08-18.
 */
@Entity
@Table(name = "gas_vehicles")
@NamedQueries({
        @NamedQuery(name = "GasVehicle.getAll", query = "SELECT a FROM GasVehicle a"),
        @NamedQuery(name="GasVehicle.getByCustomerId", query = "SELECT a FROM GasVehicle a WHERE a.owner = :owner"),
        @NamedQuery(name="GasVehicle.getByVehicle",
                query = "SELECT a FROM GasVehicle a WHERE a.baseVehicle = :vehicle")
})
public class GasVehicle {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @JoinColumn
    @OneToOne(cascade=CascadeType.REMOVE)
    private Vehicle baseVehicle;

    @Column(name = "oil_type")
    private String oilType;

    @ManyToOne
    @JoinColumn(name = "owner_id")
    private Customer owner;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Vehicle getBaseVehicle() {
        return baseVehicle;
    }

    public void setBaseVehicle(Vehicle baseVehicle) {
        this.baseVehicle = baseVehicle;
    }

    public String getOilType() {
        return oilType;
    }

    public void setOilType(String oilType) {
        this.oilType = oilType;
    }

    public Customer getOwner() {
        return owner;
    }

    public void setOwner(Customer owner) {
        this.owner = owner;
    }
}
