package com.nickross.autoservicemanager.vehicle.gas.model;

import com.nickross.autoservicemanager.customer.model.Customer;
import com.nickross.autoservicemanager.data.BaseDAO;
import com.nickross.autoservicemanager.vehicle.model.Vehicle;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

/**
 * @author Nick Ross on 2018-08-18.
 */
@Repository
public class GasVehicleDAO extends BaseDAO<GasVehicle> {

    @Override
    public List<GasVehicle> getAll() {
        List<GasVehicle> results = entityManager.createNamedQuery("GasVehicle.getAll", GasVehicle.class)
                .getResultList();
        return results;
    }

    @Override
    public Optional<GasVehicle> getById(Long id) {
        return Optional.ofNullable(entityManager.find(GasVehicle.class, id));
    }

    public List<GasVehicle> getByCustomerId(Customer owner) {
        List<GasVehicle> results;
        results = entityManager.createNamedQuery("GasVehicle.getByCustomerId", GasVehicle.class)
                .setParameter("owner", owner)
                .getResultList();
        return results;
    }

    public GasVehicle getByVehicle(Vehicle v) {
        return entityManager
                .createNamedQuery("GasVehicle.getByVehicle", GasVehicle.class)
                .setParameter("vehicle", v)
                .getSingleResult();
    }
}
