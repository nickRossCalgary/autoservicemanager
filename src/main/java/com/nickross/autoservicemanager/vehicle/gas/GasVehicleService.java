package com.nickross.autoservicemanager.vehicle.gas;

import com.nickross.autoservicemanager.customer.CustomerService;
import com.nickross.autoservicemanager.customer.model.Customer;
import com.nickross.autoservicemanager.exceptions.ValidationException;
import com.nickross.autoservicemanager.vehicle.BaseVehicleHelper;
import com.nickross.autoservicemanager.vehicle.gas.model.GasVehicle;
import com.nickross.autoservicemanager.vehicle.gas.model.GasVehicleDAO;
import com.nickross.autoservicemanager.vehicle.gas.model.GasVehicleDTO;
import com.nickross.autoservicemanager.vehicle.model.Vehicle;
import com.nickross.autoservicemanager.vehicle.model.VehicleDAO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

/**
 * @author Nick Ross on 2018-08-19.
 */
@Service
public class GasVehicleService {
    @Autowired
    private GasVehicleDAO gasVehicleDAO;
    @Autowired
    private VehicleDAO vehicleDAO;
    @Autowired
    private CustomerService customerService;

    @Transactional
    public void addGasVehicle(Long customerId, GasVehicleDTO gasVehicleDTO) throws ValidationException {
        Customer owner = customerService.validateOwner(customerId);
        GasVehicleHelper.validate(gasVehicleDTO);

        Vehicle vehicle = BaseVehicleHelper.extractBaseVehicle(gasVehicleDTO);
        GasVehicle gasVehicle = new GasVehicle();
        gasVehicle.setBaseVehicle(vehicle);
        gasVehicle.setOwner(owner);
        gasVehicle.setOilType(gasVehicleDTO.getOilType());

        vehicleDAO.create(vehicle);
        gasVehicleDAO.create(gasVehicle);
    }

    public List<GasVehicleDTO> getByCustomerId(Customer owner) {
        List<GasVehicle> gas = gasVehicleDAO.getByCustomerId(owner);
        List<GasVehicleDTO> results = new ArrayList<>();
        gas.forEach(g -> results.add(GasVehicleHelper.gasVehicleEntityToDto(g)));
        return results;
    }

    public Optional<GasVehicleDTO> getByVehicle(Vehicle v) {
        try {
            return Optional.ofNullable(GasVehicleHelper.gasVehicleEntityToDto(gasVehicleDAO.getByVehicle
                    (v)));
        } catch (EmptyResultDataAccessException e) {
            return Optional.empty();
        }
    }

    public Optional<GasVehicle> getByVehicleRaw(Vehicle v) {
        try {
            return Optional.ofNullable(gasVehicleDAO.getByVehicle(v));
        } catch (EmptyResultDataAccessException e) {
            return Optional.empty();
        }
    }

    public void removeVehicle(GasVehicle e) {
        gasVehicleDAO.delete(e);
    }
}
