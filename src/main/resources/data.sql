-- Pre-initialize effectively static services table
DROP TABLE services;
CREATE TABLE services (
 id int not null,
 title varchar(300),
 gas_specific BOOLEAN,
 electric_specific BOOLEAN,
 diesel_specific BOOLEAN
);

INSERT INTO services (id, title, gas_specific, electric_specific, diesel_specific)
              values (1, 'Tire Rotation', FALSE, FALSE, FALSE);
INSERT INTO services (id, title, gas_specific, electric_specific, diesel_specific)
              values (2, 'Engine Oil Change', TRUE, FALSE, TRUE);
INSERT INTO services (id, title, gas_specific, electric_specific, diesel_specific)
              values (3, 'Ignition Coil Check', TRUE, FALSE, FALSE);
INSERT INTO services (id, title, gas_specific, electric_specific, diesel_specific)
              values (4, 'Diesel filter change', FALSE, FALSE, TRUE);
